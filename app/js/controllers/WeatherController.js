/**
 * Created by pgoostree on 10/30/2014.
 */
'use strict';

weatherApp.controller('WeatherController',
    function WeatherController($scope, weatherData) {

        var cities = [
            'http://api.wunderground.com/api/625172310aff38a6/conditions/q/OR/Portland.json?callback=JSON_CALLBACK',
            'http://api.wunderground.com/api/625172310aff38a6/conditions/q/OR/Cannon_Beach.json?callback=JSON_CALLBACK'
        ];

        weatherData.getWeather(cities).then(
            function(data){
                $scope.weather = data;
            },
            function(data){
                console.log(data.error());
            }
        );
    }
);