/**
 * Created by pgoostree on 10/30/2014.
 */
'use strict';

weatherApp.factory('weatherData', function ($q, $http) {
    return {
        getWeather: function (cities) {
            var deferred = $q.defer();
            var results = [];

            $.each(cities, function(i, e){

                $http.jsonp(e)
                    .success(function(data){
                        results.push(data);
                    });

            });
            deferred.resolve(results);
            return deferred.promise;
        }
    };
});