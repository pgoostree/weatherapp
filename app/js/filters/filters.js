/**
 * Created by petergoostree on 10/30/14.
 */

weatherApp.filter('degreesDiff', function(){
    return function(cities){

        var temps = [];
        $.each(cities, function(i, e){
            temps.push(e.current_observation.temp_f);
        });

        var sorted = temps.sort(function(a, b){
            return b - a;
        });

        return Math.abs(sorted[0] - sorted[1]);
    }
});
